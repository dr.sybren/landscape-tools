# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
#  Copyright (C) 2014-2018 Blender Foundation
#
# ##### END GPL LICENSE BLOCK #####

# Contributor(s): Sybren A. Stüvel, Keith "Wahooney" Boshoff, Campbell Barton
# <pep8 compliant>

import enum
import typing
from math import pi

import bpy
from bpy.types import Operator
from bpy.props import FloatProperty, IntProperty, BoolProperty, EnumProperty, FloatVectorProperty


class ConvexConcave(enum.Enum):
    ALL = 0
    CONVEX = 1  # Bump
    CONCAVE = 2  # Crevice


class SkyGroundFacing(enum.Enum):
    ALL = 0
    SKY = 1  # Receives snow
    GROUND = 2  # Supports icicles


Color = typing.Tuple[float]


def compute_vertex_angles(me, blur_iterations: int, blur_strength: float):
    from mathutils import Vector
    from math import acos
    import array

    vert_ang = array.array("f", [0.0]) * len(me.vertices)

    # create lookup table for each vertex's connected vertices (via edges)
    con = [[] for _ in range(len(me.vertices))]

    # add connected verts
    for e in me.edges:
        con[e.vertices[0]].append(e.vertices[1])
        con[e.vertices[1]].append(e.vertices[0])

    use_paint_mask = me.use_paint_mask
    for i, v in enumerate(me.vertices):
        if use_paint_mask and not v.select:
            continue
        vec = Vector()
        no = v.normal
        co = v.co

        # get the direction of the vectors between the vertex and it's connected vertices
        for c in con[i]:
            vec += (me.vertices[c].co - co).normalized()

        # average the vector by dividing by the number of connected verts
        tot_con = len(con[i])
        if tot_con == 0:
            continue
        vec /= tot_con

        # angle is the acos() of the dot product between normal and connected verts.
        # > 90 degrees: convex
        # < 90 degrees: concave
        vert_ang[i] = acos(no.dot(vec))

    # blur angles
    for i in range(blur_iterations):
        # backup the original tones
        orig_vert_tone = vert_ang[:]

        # use connected verts look up for blurring
        for j, c in enumerate(con):
            for v in c:
                vert_ang[j] += blur_strength * orig_vert_tone[v]

            vert_ang[j] /= len(c) * blur_strength + 1
        del orig_vert_tone

    return vert_ang


def get_vcolor_layer_data(me, default_layer_name: str):
    for lay in me.vertex_colors:
        if lay.active:
            return lay.data

    lay = me.vertex_colors.new(default_layer_name)
    lay.active = True
    return lay.data


def get_weight_layer(ob, default_layer_name: str) -> bpy.types.VertexGroup:
    try:
        return ob.vertex_groups[ob.vertex_groups.active_index]
    except IndexError:
        pass

    lay = ob.vertex_groups.new(default_layer_name)
    ob.vertex_groups.active_index = len(ob.vertex_groups) - 1
    return lay


def apply_paint_crevices(ob,
                         mode: str,
                         concon: ConvexConcave,
                         blur_iterations: int,
                         blur_strength: float,
                         threshold_angle: float,
                         smooth_angle: float,
                         *,
                         color: Color = None):
    mesh = ob.data
    vert_ang = compute_vertex_angles(mesh, blur_iterations, blur_strength)

    # The angle is always between 0 and 180 degrees.
    # < 90 degrees means concave
    # > 90 degrees means convex
    # The threshold is there to have a third option, 'flat'
    halfway = pi / 2
    smooth_angle = min(threshold_angle, smooth_angle)
    concave_target = halfway - threshold_angle
    convex_target = halfway + threshold_angle
    concave_max_angle = concave_target + smooth_angle
    convex_min_angle = convex_target - smooth_angle

    # Create a map from vertex to interpretation; vertices are visited for each
    # of their loops, so it's useful to precompute.
    paint_convex = concon in {ConvexConcave.CONVEX, ConvexConcave.ALL}
    paint_concave = concon in {ConvexConcave.CONCAVE, ConvexConcave.ALL}
    interpretation = {}
    for vidx, ang in enumerate(vert_ang):
        if ang <= concave_max_angle:
            if not paint_concave:
                continue
            if smooth_angle and ang > concave_target:
                dist_from_target = ang - concave_target
                alpha = max(0, 1 - dist_from_target / smooth_angle)
            else:
                alpha = 1
            interpretation[vidx] = (ConvexConcave.CONCAVE, alpha)
        elif ang >= convex_min_angle:
            if not paint_convex:
                continue
            if smooth_angle and ang < convex_target:
                dist_from_target = convex_target - ang
                alpha = max(0, 1 - dist_from_target / smooth_angle)
            else:
                alpha = 1
            interpretation[vidx] = (ConvexConcave.CONVEX, alpha)

    use_paint_mask = mesh.use_paint_mask
    if mode == 'PAINT_VERTEX':
        # Paint the vertex colour map.
        vcolor_layer = get_vcolor_layer_data(mesh, 'Bumps-Crevices')
        for i, p in enumerate(mesh.polygons):
            if use_paint_mask and not p.select:
                continue
            for loop_index in p.loop_indices:
                loop = mesh.loops[loop_index]
                vidx = loop.vertex_index

                try:
                    concon, alpha = interpretation[vidx]
                except KeyError:
                    continue

                col = vcolor_layer[loop_index].color
                one_minus_alpha = (1 - alpha)
                col[0] = col[0] * one_minus_alpha + color[0] * alpha
                col[1] = col[1] * one_minus_alpha + color[1] * alpha
                col[2] = col[2] * one_minus_alpha + color[2] * alpha

    elif mode == 'PAINT_WEIGHT':
        # Paint the weight map.
        weight_layer = get_weight_layer(ob, 'Bumps-Crevices')
        for vidx, v in enumerate(mesh.vertices):
            if use_paint_mask and not v.select:
                continue
            try:
                concon, alpha = interpretation[vidx]
            except KeyError:
                continue

            # Get the existing weight
            try:
                w = weight_layer.weight(vidx)
            except RuntimeError:
                # Vertex is not a part of this group yet, so treat as weight 0
                w = 0

            new_w = w * (1 - alpha) + 1 * alpha
            weight_layer.add([vidx], new_w, 'REPLACE')

    mesh.update()
    return True


def apply_paint_skyground(ob,
                          mode: str,
                          skyground: SkyGroundFacing,
                          min_dot: float, max_dot: float,
                          *,
                          color: Color = None,
                          ) -> bool:
    import collections
    from mathutils import Vector

    mesh = ob.data
    mesh.calc_normals_split()

    # Remove translation component, as we don't need it for normal calculations.
    mat = ob.matrix_world.to_3x3()

    if max_dot <= min_dot:
        min_dot = max_dot - 0.001
    dot_range = 1 / (max_dot - min_dot)

    world_up = Vector((0, 0, 1))
    paint_skyfacing = skyground in {SkyGroundFacing.SKY, SkyGroundFacing.ALL}
    paint_groundfacing = skyground in {SkyGroundFacing.GROUND, SkyGroundFacing.ALL}

    vcolor_layer = get_vcolor_layer_data(mesh, 'Sky-Ground Facing')
    weight_layer = get_weight_layer(ob, 'Sky-Ground Facing')

    # mapping from vertex index to list of weights for each face.
    weights = collections.defaultdict(list)

    use_paint_mask = mesh.use_paint_mask
    for i, p in enumerate(mesh.polygons):
        if use_paint_mask and not p.select:
            continue
        for loop_index in p.loop_indices:
            loop = mesh.loops[loop_index]
            no_o = loop.normal  # normal in object coordinates
            no_w = mat * no_o  # normal in world coordinates

            dot = no_w.dot(world_up)
            if dot < 0:
                if not paint_groundfacing:
                    continue
                absdot = -dot
            elif dot > 0:
                if not paint_skyfacing:
                    continue
                absdot = dot
            else:
                continue

            if absdot <= min_dot:
                continue

            if absdot < max_dot:
                alpha = (absdot - min_dot) * dot_range
            else:
                alpha = 1

            if mode == 'PAINT_VERTEX':
                col = vcolor_layer[loop_index].color
                one_minus_alpha = (1 - alpha)
                col[0] = col[0] * one_minus_alpha + color[0] * alpha
                col[1] = col[1] * one_minus_alpha + color[1] * alpha
                col[2] = col[2] * one_minus_alpha + color[2] * alpha
            elif mode == 'PAINT_WEIGHT':
                # Just collect the alpha values based on each face,
                # so we can average those later.
                weights[loop.vertex_index].append(alpha)

    if mode == 'PAINT_WEIGHT':
        # Actually apply the collected alpha values to each vertex.
        for vidx, alphas in weights.items():
            alpha = sum(alphas) / len(alphas)
            weight_layer.add([vidx], alpha, 'REPLACE')

    mesh.free_normals_split()
    mesh.update()

    return True


class LANDSCAPE_PT_panel(bpy.types.Panel):
    bl_idname = 'landscape.paint'
    bl_label = 'Paint'
    bl_category = 'Landscape'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    def draw(self, context):
        layout = self.layout

        if context.mode == 'PAINT_VERTEX':
            layout.operator(PAINT_OT_vertex_color_crevices.bl_idname)
            layout.operator(PAINT_OT_vertex_color_skyground.bl_idname)
        elif context.mode == 'PAINT_WEIGHT':
            layout.operator(PAINT_OT_vertex_weight_crevices.bl_idname)
            layout.operator(PAINT_OT_vertex_weight_skyground.bl_idname)


class OperatorMixin:
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj and obj.type == 'MESH'


class VertexColorMixin(OperatorMixin):
    color = FloatVectorProperty(
        name="Color",
        default=(1.0, 1.0, 1.0),
        subtype='COLOR',
        size=3,
    )

    @classmethod
    def poll(cls, context):
        return context.mode == 'PAINT_VERTEX' and super().poll(context)

    def execute(self, context):
        return self.paint(context, color=self.color)


class VertexWeightMixin(OperatorMixin):
    @classmethod
    def poll(cls, context):
        return context.mode == 'PAINT_WEIGHT' and super().poll(context)

    def execute(self, context):
        return self.paint(context)


class PaintCrevicesMixin:
    bl_label = 'Bumps & Crevices'

    what = EnumProperty(
        name="What",
        items=[
            (ConvexConcave.CONVEX.name, 'Bumps', 'Only paint bumps'),
            (ConvexConcave.CONCAVE.name, 'Crevices', 'Only paint crevices'),
            (ConvexConcave.ALL.name, 'Both', 'Paint both bumps and crevices'),
        ],
        default=ConvexConcave.CONCAVE.name,
    )
    blur_iterations = IntProperty(
        name="Blur Iterations",
        description="Number of times to blur the colors (higher blurs more)",
        min=0, max=40,
        default=0,
    )
    blur_strength = FloatProperty(
        name="Blur Strength",
        description="Blur strength per iteration",
        min=0.01, max=1.0,
        default=1.0,
    )
    threshold = FloatProperty(
        name="Threshold Angle",
        description='Deviation from "flat" before considered bump/crevice; 0° is fully flat',
        min=0, max=pi / 4,
        step=25,
        default=2 * pi / 180,
        unit="ROTATION",
    )
    smoothness = FloatProperty(
        name="Smoothness",
        description='0=hard threshold; larger values cause smoothing between '
                    '"flat" and the threshold angle',
        min=0, max=1,
        default=0,
    )

    def paint(self, context, **kwargs):
        obj = context.object
        concon = ConvexConcave[self.what]
        smooth_angle = self.smoothness * self.threshold

        ok = apply_paint_crevices(obj, context.mode, concon,
                                  self.blur_iterations, self.blur_strength,
                                  self.threshold, smooth_angle,
                                  **kwargs)

        return {'FINISHED' if ok else 'CANCELLED'}


class PaintSkyroundMixin:
    bl_label = 'Sky & Ground Facing'
    bl_description = 'Paint sky-facing or ground-facing faces'

    what = EnumProperty(
        name="What",
        items=[
            (SkyGroundFacing.GROUND.name, 'Ground-Facing', 'Only paint ground-facing faces'),
            (SkyGroundFacing.SKY.name, 'Sky-Facing', 'Only paint sky-facing faces'),
            (SkyGroundFacing.ALL.name, 'Both', 'Paint both sky- and ground-facing'),
        ],
        default=SkyGroundFacing.SKY.name,
    )
    offset = FloatProperty(
        name="Offset",
        description="Determines how much is painted",
        min=0, max=1,
        default=0.5,
    )
    sharpness = FloatProperty(
        name="Sharpness",
        description="Determines the falloff rate of the painted area",
        min=0, max=1,
        default=0.25,
    )

    def paint(self, context, **kwargs):
        # Inversion is easier for the artists -- from their point of view,
        # a larger offset produces a larger painted area.
        dot_middle = 1 - self.offset
        dot_range = 1 - self.sharpness
        min_dot = max(0, dot_middle - dot_range)
        max_dot = min(1, dot_middle + dot_range)

        obj = context.object
        skyground = SkyGroundFacing[self.what]
        ok = apply_paint_skyground(obj, context.mode, skyground, min_dot, max_dot, **kwargs)

        return {'FINISHED' if ok else 'CANCELLED'}


class PAINT_OT_vertex_color_crevices(PaintCrevicesMixin, VertexColorMixin, Operator):
    bl_idname = 'paint.vertex_color_crevices'


class PAINT_OT_vertex_weight_crevices(PaintCrevicesMixin, VertexWeightMixin, Operator):
    bl_idname = 'paint.vertex_weight_crevices'


class PAINT_OT_vertex_color_skyground(PaintSkyroundMixin, VertexColorMixin, Operator):
    bl_idname = 'paint.vertex_color_skyground'


class PAINT_OT_vertex_weight_skyground(PaintSkyroundMixin, VertexWeightMixin, Operator):
    bl_idname = 'paint.vertex_weight_skyground'


classes = [
    LANDSCAPE_PT_panel,
    PAINT_OT_vertex_color_crevices,
    PAINT_OT_vertex_weight_crevices,
    PAINT_OT_vertex_color_skyground,
    PAINT_OT_vertex_weight_skyground,
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        try:
            bpy.utils.unregister_class(cls)
        except RuntimeError:
            print('Error unregistering class %s' % cls)
