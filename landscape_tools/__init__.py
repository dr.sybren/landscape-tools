# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
#  Copyright (C) 2014-2018 Blender Foundation
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

bl_info = {
    'name': 'Landscape Tools',
    'author': 'Sybren A. Stüvel',
    'version': (0, 0, 0),
    'blender': (2, 79, 0),
    'location': 'Add-on preferences',
    'description': 'For Andy',
    'category': 'Mesh',
}

import bpy

if 'maps' in locals():
    import importlib

    # noinspection PyUnboundLocalVariable
    maps = importlib.reload(maps)
else:
    from . import maps


def register():
    maps.register()


def unregister():
    bpy.utils.unregister_module(__name__)
    maps.unregister()
