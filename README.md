# Landscape Tools Add-on

For Andy.


## Building and Installing

* To build the addon, run `python3 setup.py bdist`
* You can install the addon from Blender
  (User Preferences → Addons → Install from file...) by pointing it to
  `dist/landscape-tools*.addon.zip`.


## Using the addon

* Install the addon as described above.
* Enable the addon in User Preferences → Addons → Mesh.
